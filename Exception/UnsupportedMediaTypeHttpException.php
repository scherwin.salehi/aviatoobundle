<?php
namespace AviatooBundle\Exception;

use AviatooBundle\Exception\Interfaces\ApiExceptionInterface;


/**
 * Class UnsupportedMediaTypeHttpException
 * @package AviatooBundle\Exception
 */
class UnsupportedMediaTypeHttpException extends \Symfony\Component\HttpKernel\Exception\UnsupportedMediaTypeHttpException implements ApiExceptionInterface
{
    /**
     * @return int
     */
    public function getStatusCode(): int
    {
        return parent::getStatusCode();
    }

    /**
     * @return array|null
     */
    public function getErrorData(): ?array
    {
        return [];
    }


}