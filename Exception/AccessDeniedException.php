<?php

namespace AviatooBundle\Exception;


use AviatooBundle\Exception\Base\ApiException;

/**
 * Class AccessDeniedException
 * @package AviatooBundle\Exception
 */
class AccessDeniedException extends ApiException
{
    /**
     * AccessDeniedException constructor.
     * @param array $errorData
     */
    public function __construct(array $errorData=[])
    {
        parent::__construct(403, $errorData, "Access Denied for User");
    }

}