<?php

namespace AviatooBundle\Exception;


use AviatooBundle\Entity\Interfaces\EntityInterface;
use AviatooBundle\Exception\Interfaces\ApiExceptionInterface;

/**
 * Class EntityNotFoundException
 * @package AviatooBundle\Exception
 */
class EntityNotFoundException extends \Doctrine\ORM\EntityNotFoundException implements ApiExceptionInterface
{
    /**
     * EntityNotFoundException constructor.
     * @param EntityInterface $entity
     */
    public function __construct(EntityInterface $entity)
    {
        parent::__construct(
            'Entity of type \'' . get_class($entity) . '\'' . ($entity->getId() ? ' for ID ' . $entity->getId() : '') . ' was not found',
            404);
    }

    /**
     * @return int
     */
    public function getStatusCode(): int
    {
        return $this->code;
    }

    /**
     * @return array|null
     */
    public function getErrorData() : ?array
    {
        return ["Entity not found"];
    }
}