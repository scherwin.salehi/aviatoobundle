<?php

namespace AviatooBundle\Exception;

use AviatooBundle\Exception\Interfaces\ApiExceptionInterface;

/**
 * Class ForeignKeyConstraintViolationException
 * @package AviatooBundle\Exception
 */
class ForeignKeyConstraintViolationException extends \Doctrine\DBAL\Exception\ForeignKeyConstraintViolationException implements ApiExceptionInterface
{
    /**
     * ForeignKeyConstraintViolationException constructor.
     * @param \Doctrine\DBAL\Exception\ForeignKeyConstraintViolationException $exception
     */
    public function __construct(\Doctrine\DBAL\Exception\ForeignKeyConstraintViolationException $exception)
    {
        parent::__construct($exception->getMessage(), $exception->getPrevious());
    }

    /**
     * @return int
     */
    public function getStatusCode(): int
    {
        return 409;
    }

    /**
     * @return array|null
     */
    public function getErrorData(): ?array
    {
        return [];
    }

}