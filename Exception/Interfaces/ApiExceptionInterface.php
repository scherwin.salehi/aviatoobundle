<?php
namespace AviatooBundle\Exception\Interfaces;

/**
 * Interface ApiExceptionInterface
 * @package AviatooBundle\Exception\Interfaces
 */
interface ApiExceptionInterface
{
    /**
     * @return int
     */
    public function getStatusCode(): int;

    /**
     * @return string
     */
    public function getMessage();

    /**
     * @return null|array
     */
    public function getErrorData(): ?array;
}
