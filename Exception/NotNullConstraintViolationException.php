<?php

namespace AviatooBundle\Exception;
use AviatooBundle\Exception\Interfaces\ApiExceptionInterface;

/**
 * Class NotNullConstraintViolationException
 * @package AviatooBundle\Exception
 */
class NotNullConstraintViolationException extends \Doctrine\DBAL\Exception\NotNullConstraintViolationException implements ApiExceptionInterface
{
    /**
     * NotNullConstraintViolationException constructor.
     * @param \Doctrine\DBAL\Exception\NotNullConstraintViolationException $exception
     */
    public function __construct(\Doctrine\DBAL\Exception\NotNullConstraintViolationException $exception)
    {
        parent::__construct($exception->getMessage(), $exception->getPrevious());
    }

    /**
     * @return int
     */
    public function getStatusCode(): int
    {
        return 409;
    }

    /**
     * @return array|null
     */
    public function getErrorData(): ?array
    {
        return [];
    }
}