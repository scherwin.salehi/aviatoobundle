<?php
namespace AviatooBundle\Exception\JWT;
use AviatooBundle\Exception\Base\ApiException;

/**
 * Class JWTNotFoundException
 * @package AviatooBundle\Exception\JWT
 */
class JWTNotFoundException extends ApiException
{
    const MESSAGE = 'JWT Token not found';
    const STATUS_CODE = 401;

    /**
     * JWTNotFoundException constructor.
     */
    public function __construct() {
        parent::__construct(self::STATUS_CODE, [],self::MESSAGE);
    }
}
