<?php
namespace AviatooBundle\Exception\JWT;
use AviatooBundle\Exception\Base\ApiException;

/**
 * Class JWTExpiredException
 * @package AviatooBundle\Exception\JWT
 */
class JWTExpiredException extends ApiException
{
    const MESSAGE = 'JWT expired!';
    const STATUS_CODE = 401;

    /**
     * JWTExpiredException constructor.
     */
    public function __construct() {
        parent::__construct(self::STATUS_CODE, [],self::MESSAGE);
    }
}
