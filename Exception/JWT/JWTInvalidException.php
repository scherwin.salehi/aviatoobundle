<?php
namespace AviatooBundle\Exception\JWT;
use AviatooBundle\Exception\Base\ApiException;

/**
 * Class JWTInvalidException
 * @package AviatooBundle\Exception\JWT
 */
class JWTInvalidException extends ApiException
{

    const MESSAGE = 'Invalid JWT Token';
    const STATUS_CODE = 401;

    /**
     * InvalidJWTException constructor.
     */
    public function __construct() {
        parent::__construct(self::STATUS_CODE, [],self::MESSAGE);
    }
}
