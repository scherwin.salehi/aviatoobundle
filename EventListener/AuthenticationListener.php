<?php
namespace AviatooBundle\EventListener;

use AviatooBundle\Exception\JWT\InvalidLoginException;
use AviatooBundle\Exception\JWT\JWTInvalidException;
use AviatooBundle\Exception\JWT\JWTExpiredException;
use AviatooBundle\Exception\JWT\JWTNotFoundException;

/**
 * Class AuthenticationListener
 * @package AviatooBundle\EventListener
 */
class AuthenticationListener
{
    /**
     * @throws InvalidLoginException
     */
    public function onAuthenticationFailureResponse()
    {
        throw new InvalidLoginException();
    }

    /**
     * @throws JWTInvalidException
     */
    public function onJWTInvalid()
    {
        throw new JWTInvalidException();
    }

    /**
     * @throws JWTNotFoundException
     */
    public function onJWTNotFound()
    {
        throw new JWTNotFoundException();
    }

    /**
     * @throws JWTExpiredException
     */
    public function onJWTExpired()
    {
        throw new JWTExpiredException();
    }
}
