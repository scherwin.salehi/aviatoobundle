<?php

namespace AviatooBundle\Controller\Traits;


use AviatooBundle\Entity\Interfaces\EntityInterface;
use AviatooBundle\Entity\Interfaces\ImageInterface;
use Symfony\Component\Validator\Constraints as Assert;

trait S3Trait
{
    protected function getS3(){
        return $this->get("aws.s3");
    }

    protected function uploadS3(ImageInterface $image){
        if($image->getName() === null){
            $this->getHashGenerator()->setEntity($image);
            $this->getHashGenerator()->setField("name");
            $this->getHashGenerator()->generateHash(true);
            $this->getHashGenerator()->flush();
        }
        /** @var ImageInterface $image */
        if($image instanceof EntityInterface) $image=$this->getDoctrine()->getRepository("AviatooBundle:Image")->find($image->getId());
        $this->getS3()->upload($this->getParameter("aws_bucket"),$image->getPath(),file_get_contents($image->getFile(),"public-read"));
        return $image;
    }

    protected function deleteS3(ImageInterface $image){
        $this->getS3()->deleteObject([
            "Bucket"=> $this->getParameter("aws_bucket"),
            "Key"=>$image->getPath()
        ]);
    }
}