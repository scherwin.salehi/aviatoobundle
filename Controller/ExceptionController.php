<?php
namespace AviatooBundle\Controller;

/**
 * Class ExceptionController
 * @package AviatooBundle\Controller
 */
class ExceptionController extends BaseController
{
    /**
     * @param \Exception $exception
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function showAction(\Exception $exception)
    {
        $response = $this->getControllerResponseFactory()->createWithException($exception);
        return $this->getResponseFactory()->createFromControllerResponse($response);
    }
}
