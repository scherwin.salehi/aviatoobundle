<?php

namespace AviatooBundle\Repository;

use JMS\Serializer\Annotation as Serializer;
use JMS\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\ORM\QueryBuilder;


class Pagination
{

    private static $pagination;

    public static function buildPagination($page,$page_size){
        self::$pagination = new Pagination($page,$page_size);
    }

    /**
     * @return Pagination $pagination
     */
    public static function getPagination(){
        return self::$pagination;
    }

    public function __construct($page,$page_size){
        $this->limit = $page_size;
        $this->offset= $page*$page_size;
    }

    /**
     * @param QueryBuilder $queryBuilder
     * @return mixed
     */
    public function decoratePagination($queryBuilder){
        return $queryBuilder->setMaxResults($this->getLimit())->setFirstResult($this->getOffset());
    }

    /**
     * @var int
     */
    private $maxLimit = 100;
    /**
     * @var int
     * @Serializer\Type("int")
     * @Groups({Pagination::IN})
     */
    private $offset = 0;
    /**
     * @var int
     * @Serializer\Type("int")
     * @Groups({Pagination::IN})
     */
    private $limit = 50;

    /**
     * @return int
     */
    public function getOffset()
    {
        if ($this->offset > $this->maxLimit) $this->offset = $this->maxLimit;
        return $this->offset;
    }

    /**
     * @param int $offset
     */
    public function setOffset($offset)
    {
        $this->offset = $offset;
    }

    /**
     * @return int
     */
    public function getLimit()
    {
        return $this->limit;
    }

    /**
     * @param int $limit
     */
    public function setLimit($limit)
    {
        $this->limit = $limit;
    }

    public function setMaxLimit($maxLimit)
    {
        $this->maxLimit = $maxLimit;
    }
}