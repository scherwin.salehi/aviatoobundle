<?php
namespace AviatooBundle\Repository;


use AviatooBundle\Repository\Pagination;
use Doctrine\ORM\QueryBuilder;
use Doctrine\ORM\Query\Expr;

class EntityRepository extends \Doctrine\ORM\EntityRepository
{
    public function createQueryBuilder($alias, $indexBy = null)
    {
        $queryBuidler = parent::createQueryBuilder($alias,$indexBy);
        $pagination = Pagination::getPagination();
        if($pagination instanceof Pagination){
            $queryBuidler = $pagination->decoratePagination($queryBuidler);
        }
        return $queryBuidler;
    }
}