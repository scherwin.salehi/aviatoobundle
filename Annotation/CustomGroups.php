<?php
namespace AviatooBundle\Annotation;
use JMS\Serializer\Annotation\Groups;
use AviatooBundle\Constants\GroupConstants as G;
use AviatooBundle\Annotation\Traits\GroupFetcherTrait;

/**
 * Class Parameter
 * @package AviatooBundle\Controller\Annotation
 * @Annotation
 */
class CustomGroups extends Groups
{
    use GroupFetcherTrait;

    /**
     * CustomGroups constructor.
     * @param array $options
     */
    public function __construct(array $options)
    {

        $values =[G::ENTITY_OUT,G::INDEX_OUT];
        $idGroup = $this->fetchGroup();
        if($idGroup){
            $values[]=$idGroup;
        }
        $this->groups = $values;
    }
}
