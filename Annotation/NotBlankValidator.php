<?php
namespace AviatooBundle\Annotation;
use AviatooBundle\Annotation\Traits\GroupFetcherTrait;
use Doctrine\Common\Annotations\DocParser;
use JMS\Serializer\Annotation\Groups;
use AviatooBundle\Constants\GroupConstants;

/**
 * Class Parameter
 * @package AviatooBundle\Annotation
 */
class NotBlankValidator extends \Symfony\Component\Validator\Constraints\NotBlankValidator
{

}
