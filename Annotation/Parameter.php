<?php
namespace AviatooBundle\Annotation;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;

/**
 * Class Parameter
 * @package AviatooBundle\Controller\Annotation
 * @Annotation
 */
class Parameter extends ParamConverter
{
    /**
     * Parameter constructor.
     * @param array $values
     */
    public function __construct(array $values)
    {
        $value = $values['value'];
        $name = $value[0];
        $groups = $value[1];

        parent::__construct([
            'name' => $name,
            'converter' => 'json.param_converter',

            'options' => [
                'validator' => [
                    'groups' => $groups
                ],
                'deserializationContext' => [
                    'groups' => $groups
                ]
            ]
        ]);
    }
}
