<?php
namespace AviatooBundle\Annotation;

/**
 * Class Parameter
 * @package AviatooBundle\Controller\Annotation
 * @Annotation
 * @Target("METHOD")
 */
class Pagination
{
    protected $page_size=15;

    /**
     * Parameter constructor.
     * @param array $values
     */
    public function __construct(array $values)
    {
        if(array_key_exists("page_size",$values)) $this->page_size = $values["page_size"];
    }

    /**
     * @return mixed
     */
    public function getPageSize()
    {
        return $this->page_size;
    }

    /**
     * @param mixed $pageSize
     */
    public function setPageSize($pageSize)
    {
        $this->page_size = $pageSize;
    }

}
