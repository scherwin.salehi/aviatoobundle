<?php
namespace AviatooBundle\Annotation;
use AviatooBundle\Annotation\Traits\GroupFetcherTrait;
use Doctrine\Common\Annotations\DocParser;
use JMS\Serializer\Annotation\Groups;
use AviatooBundle\Constants\GroupConstants;

/**
 * Class Parameter
 * @package AviatooBundle\Controller\Annotation
 * @Annotation
 */
class NotBlank extends \Symfony\Component\Validator\Constraints\NotBlank
{
    use GroupFetcherTrait;
    /**
     * NotBlank constructor.
     * @param mixed|null $options
     */
    public function __construct($options)
    {

        $idGroup = $this->fetchGroup();

        parent::__construct([
            "groups"=>[$idGroup]
        ]);
    }

}
