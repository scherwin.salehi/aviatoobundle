<?php
namespace AviatooBundle\Annotation\Traits;

use Doctrine\Common\Annotations\DocParser;


trait GroupFetcherTrait{


    private function fetchGroup(){
        $docParser = debug_backtrace()[2]["object"];
        $context = false;
        if($docParser instanceof DocParser){
            $getContext= \Closure::bind(function (DocParser $docParser){
                return $docParser->context;
            },null,$docParser);
            $context = $getContext($docParser);
            $context = str_replace(["property ",'::$id'],["",""],$context);
        }
        if($context && defined($context."::PARAM_CONV")){
            return $context::PARAM_CONV;
        }
        return false;
    }
}