<?php
namespace AviatooBundle\Auth;

use AviatooBundle\Constants\GroupConstants;
use AviatooBundle\Entity\User;
use AviatooBundle\Service\Response\ControllerResponseFactory;
use AviatooBundle\Service\Response\ResponseFactory;
use Lexik\Bundle\JWTAuthenticationBundle\Services\JWTManager;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;

/**
 * Class AuthenticationSuccessHandler
 * @package AviatooBundle\Service\User
 */
class AuthenticationSuccessHandler extends \Lexik\Bundle\JWTAuthenticationBundle\Security\Http\Authentication\AuthenticationSuccessHandler
{
    /** @var  ControllerResponseFactory $controllerResponseFactory */
    private $controllerResponseFactory;

    /** @var  ResponseFactory $responseFactory */
    private $responseFactory;

    /**
     * AuthenticationSuccessHandler constructor.
     * @param ControllerResponseFactory $controllerResponseFactory
     * @param ResponseFactory $responseFactory
     * @param JWTManager $jwtManger
     * @param EventDispatcherInterface $dispatcher
     */
    public function __construct(
        ControllerResponseFactory $controllerResponseFactory,
        ResponseFactory $responseFactory,
        JWTManager $jwtManger,
        EventDispatcherInterface $dispatcher
    ) {
        $this->responseFactory = $responseFactory;
        $this->controllerResponseFactory = $controllerResponseFactory;

        parent::__construct($jwtManger, $dispatcher);
    }

    /**
     * Override default method to generate
     *
     * @param Request $request
     * @param TokenInterface $token
     * @return Response
     */
    public function onAuthenticationSuccess(Request $request, TokenInterface $token): Response
    {
        $response = $this->handleAuthenticationSuccess($token->getUser());
        $data = json_decode($response->getContent());

        $jwtToken = $data->token;
        $refreshToken = $data->refresh_token;

        /** @var User $user */
        $user = $token->getUser();
        $user->setJwtToken($jwtToken);
        $user->setRefreshToken($refreshToken);

        $data = ['user' => $user];

        $controllerResponse = $this->controllerResponseFactory
            ->createWithData($data, [GroupConstants::ENTITY_OUT], 'Logged in');

        return $this->responseFactory->createFromControllerResponse($controllerResponse);
    }
}
