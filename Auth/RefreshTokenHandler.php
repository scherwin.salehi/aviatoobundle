<?php
namespace AviatooBundle\Auth;

use AviatooBundle\Entity\User;
use Gesdinet\JWTRefreshTokenBundle\Doctrine\RefreshTokenManager;
use Gesdinet\JWTRefreshTokenBundle\EventListener\AttachRefreshTokenOnSuccessListener;
use Lexik\Bundle\JWTAuthenticationBundle\Event\AuthenticationSuccessEvent;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class RefreshTokenHandler
 * @package AviatooBundle\Service\User
 */
class RefreshTokenHandler
{
    /** @var  AttachRefreshTokenOnSuccessListener $attachRefreshTokenListener  */
    private $attachRefreshTokenListener;

    /** @var  RefreshTokenManager $refreshTokenManager */
    private $refreshTokenManager;

    /**
     * RefreshTokenHandler constructor.
     * @param AttachRefreshTokenOnSuccessListener $attachRefreshTokenListener
     * @param RefreshtokenManager $refreshTokenManager
     */
    public function __construct(AttachRefreshTokenOnSuccessListener $attachRefreshTokenListener, RefreshTokenManager $refreshTokenManager)
    {
        $this->attachRefreshTokenListener = $attachRefreshTokenListener;
        $this->refreshTokenManager = $refreshTokenManager;
    }

    /**
     * Fakes auth success event to use Refresh bundle to generate refresh token
     * @param User $user
     * @return string
     */
    public function createToken(User $user): string
    {
        $response = new Response();
        $event = new AuthenticationSuccessEvent([], $user, $response);
        $this->attachRefreshTokenListener->attachRefreshToken($event);

        $refreshToken = $this->refreshTokenManager->getLastFromUsername($user->getUsername());
        return $refreshToken->getRefreshToken();
    }

    /**
     * @param string $token
     */
    public function deleteToken(string $token)
    {
        $token = $this->refreshTokenManager->get($token);
        if ($token) {
            $this->refreshTokenManager->delete($token, true);
        }
    }
}
