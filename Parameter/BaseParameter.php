<?php

namespace AviatooBundle\Parameter;
use Symfony\Component\HttpFoundation\ParameterBag;


abstract class BaseParameter
{

    public function __construct($class,ParameterBag $parameterBag)
    {
        $reflectionProperties = (new \ReflectionClass($class))->getProperties(\ReflectionProperty::IS_PROTECTED);
        foreach($reflectionProperties as $reflectionProperty){
            $name = $reflectionProperty->getName();
            $value = $parameterBag->get($name,null);
            if($value !== null) $this->$name = $value;
        }
    }

}