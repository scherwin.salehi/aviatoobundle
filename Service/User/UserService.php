<?php
namespace AviatooBundle\Service\User;

use AviatooBundle\Auth\RefreshTokenHandler;
use AviatooBundle\Constants\UserRoles;
use AviatooBundle\Entity\User;
use AviatooBundle\Exception\UserAlreadyExistsException;
use FOS\UserBundle\Model\UserManagerInterface;
use Lexik\Bundle\JWTAuthenticationBundle\Services\JWTTokenManagerInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

class UserService
{
    /** @var  UserManagerInterface $userManager */
    private $userManager;

    /** @var  JWTTokenManagerInterface $jwtManager */
    private $jwtManager;

    /** @var  RefreshTokenHandler $refreshTokenHandler */
    private $refreshTokenHandler;

    private $tokenStorage;

    /**
     * UserService constructor.
     * @param UserManagerInterface $userManager
     * @param JWTTokenManagerInterface $jwtManager
     * @param RefreshTokenHandler $refreshTokenHandler
     */
    public function __construct(UserManagerInterface $userManager, JWTTokenManagerInterface $jwtManager, RefreshTokenHandler $refreshTokenHandler,TokenStorageInterface $tokenStorage)
    {
        $this->userManager = $userManager;
        $this->jwtManager = $jwtManager;
        $this->refreshTokenHandler = $refreshTokenHandler;
        $this->tokenStorage=$tokenStorage;
    }

    /**
     * @param User $user
     * @return User
     * @throws UserAlreadyExistsException
     */
    public function createUser(User $user): User
    {
        $existingUser = $this->userManager->findUserByEmail($user->getEmail());
        if ($existingUser) {
            throw new UserAlreadyExistsException();
        }
        $user->setPlainPassword($user->getPassword());
        $user->setEnabled(true);


        $this->userManager->updateUser($user);

        $token = $this->jwtManager->create($user);
        $refreshToken = $this->refreshTokenHandler->createToken($user);
        $user->setJwtToken($token);
        $user->setRefreshToken($refreshToken);

        return $user;
    }

    /**
     * Deletes the user's refresh token (and let JWT expire)
     * @param User $user
     * @return bool
     */
    public function logoutUser(User $user): bool
    {
        $this->refreshTokenHandler->deleteToken($user->getRefreshToken());
        return true;
    }

    /**
     * Get a user from the Security Token Storage.
     *
     * @return mixed
     *
     * @throws \LogicException If SecurityBundle is not available
     *
     * @see TokenInterface::getUser()
     *
     * @final since version 3.4
     */
    public function getUser()
    {
        if (null === $token = $this->tokenStorage->getToken()) {
            return;
        }

        if (!is_object($user = $token->getUser())) {
            // e.g. anonymous authentication
            return;
        }

        return $user;
    }
}
