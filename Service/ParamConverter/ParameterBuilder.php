<?php

namespace  AviatooBundle\Service\ParamConverter;


use AviatooBundle\Parameter\BaseParameter;
use AviatooBundle\Exception\InvalidParamsException;
use Symfony\Component\HttpFoundation\ParameterBag;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class ParameterBuilder
{
    private $validator;

    public function __construct(ValidatorInterface $validator){
        $this->validator = $validator;
    }

    /**
     * @param ParameterBag $parameterBag
     * @param $class
     * @return BaseParameter
     * @throws InvalidParamsException
     */
    public function buildParameter(ParameterBag $parameterBag,$class){
        if(class_exists($class)){
            $object = new $class($parameterBag);
            $validationErrors = $this->validator->validate($object);
            if(count($validationErrors) > 0) {
                throw new InvalidParamsException($validationErrors);
            }
            return $object;
        }
        throw new InvalidParamsException([]);

    }

}