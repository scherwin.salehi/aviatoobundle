<?php

namespace AviatooBundle\Service\ParamConverter;

use AviatooBundle\Constants\GroupConstants;
use AviatooBundle\Entity\Interfaces\EntityInterface;
use AviatooBundle\Entity\Interfaces\FileHolderInterface;
use FOS\RestBundle\Context\Context;
use FOS\RestBundle\Request\RequestBodyParamConverter;
use AviatooBundle\Service\ParamConverter\SerializerService;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class JsonParamConverter
 * @package AviatooBundle\ParamConverter
 */
class JsonParamConverter extends RequestBodyParamConverter
{
    const
        PAGINATION="pagination";
    const DEFOPTIONS = [
        "pagination"=>0,
    ];
    private $context= [];
    private $options;
    private $configuration;
    /**
     * @var SerializerService
     */
    protected $serializerService;
    private $file;
    /**
     * The name of the argument on which the ConstraintViolationList will be set.
     *
     * @var null|string
     */
    private $validationErrorsArgument;

    /**
     * JsonParamConverter constructor.
     * @param SerializerService $serializerService
     * @param null $groups
     * @param null $version
     */
    public function __construct(
        SerializerService $serializerService,
        $groups = null,
        $version = null
    ) {
        $this->serializerService=$serializerService;
        if (!empty($groups)) {
            $this->context['groups'] = (array) $groups;
        }
        if (!empty($version)) {
            $this->context['version'] = $version;
        }

        parent::__construct($serializerService->serializer,$groups,$version,$serializerService->validator,$serializerService->validationErrorsArgument);
    }

    /**
     * @param ParamConverter $configuration
     * @return bool
     */
    public function supports(ParamConverter $configuration)
    {
        return null !== $configuration->getClass() && 'json.param_converter' === $configuration->getConverter();
    }

    /**
     * @param Request $request
     * @param ParamConverter $configuration
     * @return bool
     */
    public function apply(Request $request, ParamConverter $configuration)
    {
        $options = (array) $configuration->getOptions();
        $this->configuration=$configuration;

        if (isset($options['deserializationContext']) && is_array($options['deserializationContext'])) {
            $arrayContext = array_merge($this->context, $options['deserializationContext']);
        } else {
            $arrayContext = $this->context;
        }

        $this->configureContext($context = new Context(), $arrayContext);
        $body=$this->getBody($request,$arrayContext);
        $object=$this->serializerService->deserialize($body,$configuration,$context);
        if($this->file instanceof UploadedFile && $object instanceof FileHolderInterface) $object->setFile($this->file);
        $this->serializerService->setValidationGroups($options);
        $this->serializerService->validate($object,$options);

        if($object instanceof EntityInterface){
            $object = $this->serializerService->synchronizeEntity($object);
        }

        $request->attributes->set($configuration->getName(),$object);

        return true;
    }

    private function extractGetOrFormOptions(Request $request, $isGet=true){
        if(!$isGet){
            $options = $request->request->get("options",[]);
            if($options && !is_array($options)) $options = [$options];
            return $options;
        }
        else {
            $options = [];
            foreach (array_keys(self::DEFOPTIONS) as $key){
                $val = $request->query->get($key,false);
                if($val) $options[$key] = $val;
            }
            return $options;
        }
    }

    private function buildDefaultBody(Request $request,$isGet=true){
        $key = $isGet? "query":"request";
        $defBody=["data"=>[],"options"=>self::DEFOPTIONS];

        $id=$request->$key->get("id",false);
        $content=$request->$key->get("content",false);
//        $options= $this->extractGetOrFormOptions($request,$isGet);

        if($content) {
            return base64_decode(urldecode($content));
        }
        elseif(is_numeric($id)){
            $defBody =["id"=>$id];
        }
//        if(is_array($options)){
//            $defBody["options"]=$options;
//        }

        return json_encode($defBody);
    }

    /**
     * @param Request $request
     * @param $arrayContext
     * @return mixed|resource|string
     */
    private function getBody(Request $request,$arrayContext){
        $groups=@$arrayContext["groups"];
        $fileUploaded = is_array($groups)&&array_search(GroupConstants::UPLOAD,$groups)!==false;
        $isGet = $request->getMethod() === "GET";
        if($isGet|| $fileUploaded){
            if($fileUploaded) $this->file=@$request->files->all()["file"];
            $body = $this->buildDefaultBody($request,$isGet);
        }
        else $body=$request->getContent();
        return $body;
    }

}