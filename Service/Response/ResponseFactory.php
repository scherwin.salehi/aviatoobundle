<?php
namespace AviatooBundle\Service\Response;

use FOS\RestBundle\Context\Context;
use FOS\RestBundle\View\View;
use FOS\RestBundle\View\ViewHandlerInterface;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class ResponseFactory
 * @package AviatooBundle\Service\Response
 */
class ResponseFactory
{
    /** @var  ViewHandlerInterface $viewHandler */
    private $viewHandler;

    /**
     * ResponseFactory constructor.
     * @param ViewHandlerInterface $viewHandler
     */
    public function __construct(ViewHandlerInterface $viewHandler)
    {
        $this->viewHandler = $viewHandler;
    }

    /**
     * @param ControllerResponse $controllerResponse
     * @return Response
     */
    public function createFromControllerResponse(ControllerResponse $controllerResponse): Response
    {
        $context = new Context();
        $serializationGroups = array_merge($controllerResponse->getSerializationGroups(), ['default']);
        $context->setGroups($serializationGroups);

        $view = View::create();

        //$view->setHeader('Access-Control-Allow-Origin', '*');


        $view->setStatusCode($controllerResponse->getCode());
        $view->setData($controllerResponse);
        $view->setContext($context);

        return $this->viewHandler->handle($view);
    }
}
