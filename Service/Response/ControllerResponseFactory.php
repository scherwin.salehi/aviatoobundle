<?php
namespace AviatooBundle\Service\Response;

use AviatooBundle\Exception\Interfaces\ApiExceptionInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\HttpExceptionInterface;

/**
 * Class ControllerResponseFactory
 * @package AviatooBundle\Service\Response
 */
class ControllerResponseFactory
{
    const DEFAULT_EXCEPTION_MESSAGE = "Could not handle the request";

    /** @var  string $environment */
    private $environment;

    /**
     * ControllerResponseFactory constructor.
     * @param string $environment
     */
    public function __construct(string $environment)
    {
        $this->environment = $environment;
    }

    /**
     * @param array $data
     * @param string|null $message
     * @param array $serializationGroups
     * @return ControllerResponse
     */
    public function createWithData(array $data, array $serializationGroups, string $message = null): ControllerResponse
    {
        $response = new ControllerResponse();
        $response->setMessage($message ?? 'Success!');
        $response->setData($data);
        $response->setSuccess(true);
        $response->setCode(Response::HTTP_OK);
        $response->addSerializationGroups($serializationGroups);

        return $response;
    }

    /**
     * @param \Exception $exception
     * @return ControllerResponse
     */
    public function createWithException(\Exception $exception)
    {
        $response = new ControllerResponse();
        $response->setSuccess(false);
        if ($exception instanceof ApiExceptionInterface) {
            $response->setMessage($exception->getMessage());
            $response->setCode($exception->getStatusCode());
            $response->setErrorData($exception->getErrorData());

            return $response;
        }
        $response->setCode(Response::HTTP_BAD_REQUEST);
        if ($exception instanceof HttpExceptionInterface) {
            $response->setCode($exception->getStatusCode());
        }

        $response->setMessage(self::DEFAULT_EXCEPTION_MESSAGE);
        if ($this->environment === 'dev') {
            $response->setMessage($exception->getMessage() . $exception->getTraceAsString());
        }

        return $response;
    }
}
