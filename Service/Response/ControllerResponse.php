<?php
namespace AviatooBundle\Service\Response;

use JMS\Serializer\Annotation\Groups;

/**
 * Class ControllerResponse
 * @package AviatooBundle\Service\Response
 */
class ControllerResponse
{
    const DEFAULT = "default";

    /**
     * @var  bool $success
     * @Groups({ControllerResponse::DEFAULT})
     */
    private $success;

    /**
     * @var  int $code
     * @Groups({ControllerResponse::DEFAULT})
     */
    private $code;

    /**
     * @var  string $message
     * @Groups({ControllerResponse::DEFAULT})
     */
    private $message;

    /**
     * @var  null|array $data
     * @Groups({ControllerResponse::DEFAULT})
     */
    private $data;

    /**
     * @var  null|array
     * @Groups({ControllerResponse::DEFAULT})
     */
    private $errorData;

    /** @var  array $serializationGroups */
    private $serializationGroups = [];

    /**
     * @return bool
     */
    public function isSuccess(): bool
    {
        return $this->success;
    }

    /**
     * @param bool $success
     */
    public function setSuccess(bool $success)
    {
        $this->success = $success;
    }

    /**
     * @return string
     */
    public function getMessage(): string
    {
        return $this->message;
    }

    /**
     * @param string $message
     */
    public function setMessage(string $message)
    {
        $this->message = $message;
    }

    /**
     * @return array
     */
    public function getData(): ?array
    {
        return $this->data;
    }

    /**
     * @param $data
     */
    public function setData($data)
    {
        $this->data = $data;
    }

    /**
     * @return mixed
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * @param mixed $code
     */
    public function setCode($code)
    {
        $this->code = $code;
    }

    /**
     * @return array|null
     */
    public function getErrorData(): ?array
    {
        return $this->errorData;
    }

    /**
     * @param array|null $errorData
     */
    public function setErrorData(array $errorData)
    {
        $this->errorData = $errorData;
    }

    /**
     * @return array
     */
    public function getSerializationGroups(): array
    {
        return $this->serializationGroups;
    }

    /**
     * @param array $serializationGroups
     */
    public function setSerializationGroups(array $serializationGroups)
    {
        $this->serializationGroups = $serializationGroups;
    }

    /**
     * @param array $groups
     */
    public function addSerializationGroups(array $groups)
    {
        $this->serializationGroups = array_merge($this->serializationGroups, $groups);
    }
}
