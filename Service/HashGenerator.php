<?php


namespace AviatooBundle\Service;



use AviatooBundle\Entity\Interfaces\EntityInterface;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Config\Definition\Exception\Exception;

/**
 * Class HashGenerator
 * @package AviatooBundle\Service
 */
class HashGenerator
{
    private
        /**
         * @var string
         */
        $field = "token",
        /**
         * @var int
         */
        $hashLength = 8,

        /**
         * @var EntityInterface
         */
        $entity,
        /**
         * @var EntityManager
         */
        $entityManager;

    /**
     * HashGenerator constructor.
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager){
        $this->entityManager=$entityManager;
    }

    /**
     * @param EntityInterface $entity
     */
    public function setEntity(EntityInterface $entity){
        $this->entity=$entity;
    }

    /**
     * @param $length
     */
    public function setHashLength($length){
        $this->hashLength = $length;
    }

    /**
     * @param $field
     */
    public function setField($field){
        $this->field=strtolower($field);
    }

    /**
     * @param bool $force
     * @return string
     */
    public function generateHash($force=false){
        $getter=$this->_getGetter();
        if(method_exists($this->entity,$getter) && !$force && $this->entity->$getter()) return $this->entity->$getter();

        $setter=$this->_getSetter();
        $hash=$this->getHash();
        while($this->isRedundant($hash))$hash=$this->getHash();

        if(method_exists($this->entity,$setter)){
            $this->entity->$setter($hash);
            $this->persist();
        }else throw new Exception("Method ".$setter." not defined for Class ".$this->getEntityClassName());

        return $hash;
    }

    /**
     * @return EntityInterface
     */
    public function getEntity() {
        return $this->entity;
    }

    /**
     * @param $hash
     * @return object
     */
    public function isRedundant($hash){
        return $this->getRepository()->findOneBy([$this->field=>$hash]);
    }

    /**
     * @return string
     */
    private function getEntityClassName(){
        return (new \ReflectionClass($this->entity))->getShortName();
    }

    /**
     * @return string
     */
    private function getRepositoryName(){
        return "AviatooBundle:".$this->getEntityClassName();
    }

    /**
     * @return \Doctrine\Common\Persistence\ObjectRepository
     */
    private function getRepository(){
        return $this->entityManager->getRepository($this->getRepositoryName());
    }

    /**
     * @return string
     */
    private function _getGetter(){
        return "get".ucfirst($this->field);
    }

    /**
     * @return string
     */
    private function _getSetter(){
        return "set".ucfirst($this->field);
    }

    /**
     * @return string
     */
    public function getHash(){
        $hash=substr(md5(microtime()),rand(0,26),$this->hashLength);
        return $hash;
    }

    /**
     *
     */
    private function persist(){
        $this->entityManager->persist($this->entity);
    }

    /**
     *
     */
    public function flush(){
        $this->entityManager->flush();
    }

}