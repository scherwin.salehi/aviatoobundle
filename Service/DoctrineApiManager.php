<?php

namespace AviatooBundle\Service;

use AviatooBundle\Repository\Pagination;
use Doctrine\DBAL\Exception\ForeignKeyConstraintViolationException;
use Doctrine\DBAL\Exception\NotNullConstraintViolationException;
use Doctrine\DBAL\Exception\UniqueConstraintViolationException;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\ORM\QueryBuilder;
use Doctrine\DBAL\Driver\Connection;


/**
 * Class DoctrineApiManager
 * @package AviatooBundle\Service
 */
class DoctrineApiManager
{
    const PAGE_SIZE=10;

    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * @var Connection
     */
    private $nativeConnection;

    /**
     * DoctrineApiManager constructor.
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager,Connection $connection)
    {
        $this->entityManager=$entityManager;
        $this->nativeConnection = $connection;
    }

    /**
     * @return \Doctrine\DBAL\Query\QueryBuilder
     */
    public function getNativeQueryBuilder(){
        return $this->nativeConnection->createQueryBuilder();
    }

    /**
     * @param $class
     * @param $alias
     * @param Request|null $request
     * @return QueryBuilder
     */
    public function getQueryBuilder($class,$alias){
        $queryBuilder = $this->getRepository($class)->createQueryBuilder($alias);
        $pagination = Pagination::getPagination();
        if($pagination) $queryBuilder = $pagination->decoratePagination($queryBuilder);
        return $queryBuilder;
    }

    /**
     * @param $entity
     */
    public function persist($entity)
    {
        $this->entityManager->persist($entity);
    }

    /**
     * @param $entityName
     * @return \Doctrine\Common\Persistence\ObjectRepository
     */
    public function getRepository($entityName)
    {
        return $this->entityManager->getRepository($entityName);
    }

    /**
     * @param null $entity
     * @throws \AviatooBundle\Exception\ForeignKeyConstraintViolationException
     * @throws \AviatooBundle\Exception\NotNullConstraintViolationException
     * @throws \AviatooBundle\Exception\UniqueConstraintViolationException
     * @throws \Exception
     */
    public function flush($entity = null)
    {
        try{
            $this->entityManager->flush($entity);
        }catch (\Exception $exception){
            if($exception instanceof UniqueConstraintViolationException)
                throw new \AviatooBundle\Exception\UniqueConstraintViolationException($exception);
            else if($exception instanceof ForeignKeyConstraintViolationException)
                throw new \AviatooBundle\Exception\ForeignKeyConstraintViolationException($exception);
            else if($exception instanceof  NotNullConstraintViolationException)
                throw new \AviatooBundle\Exception\NotNullConstraintViolationException($exception);
            else throw $exception;
        }

    }

    /**
     * @param $entity
     */
    public function remove($entity)
    {
        $this->entityManager->remove($entity);// TODO: Change the autogenerated stub
    }

}