<?php
namespace AviatooBundle\Entity\Traits;

use Doctrine\ORM\Mapping as ORM;
use AviatooBundle\Annotation\NotBlank;
use AviatooBundle\Annotation\CustomGroups;

/**
 * Class IdTrait
 * @package AviatooBundle\Entity\Traits
 * This Trait cannot be used for Base classes (ERBUNG GEHT HIER NICHT)
 */
trait IdTrait{

    /**
     * @NotBlank
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     * @CustomGroups({})
     */
    protected $id;

    /**
     * @return int
     */
    public function getId(){return $this->id;}

    /**
     * @param $id
     * @return $this
     */
    public function setId($id){$this->id=$id;return $this;}
}