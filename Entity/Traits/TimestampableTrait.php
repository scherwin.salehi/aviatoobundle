<?php
namespace AviatooBundle\Entity\Traits;

use AviatooBundle\Entity\Interfaces\ValidateAbleInterface;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation\Groups;
use AviatooBundle\Constants\GroupConstants as G;
/**
 * ORM\HasLifecycleCallbacks
 */
trait TimestampableTrait
{
    /**
     * @var \Datetime $created
     * @Groups({G::ENTITY_OUT,G::INDEX_OUT})
     * @ORM\Column(type="datetime")
     */
    protected $createdAt;

    /**
     * @var \DateTime $updated
     * @Groups({G::ENTITY_OUT,G::INDEX_OUT})
     * @ORM\Column(type="datetime")
     */
    protected $updatedAt;

    /**
     * Gets triggered only on insert

     * @ORM\PrePersist
     */
    public function onPrePersist()
    {
        $this->createdAt = new \DateTime();
        $this->updatedAt = new \DateTime();
        if($this instanceof ValidateAbleInterface) $this->validateInsert();

    }

    /**
     * Gets triggered every time on update

     * @ORM\PreUpdate
     */
    public function onPreUpdate()
    {
        $this->updatedAt = new \DateTime();
        if($this instanceof ValidateAbleInterface) $this->validateUpdate();
    }

    /**
     * Get createdAt
     *
     * @return datetime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set createdAt
     *
     * @param datetime $createdAt
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return datetime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set updatedAt
     *
     * @param datetime $updatedAt
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }
}