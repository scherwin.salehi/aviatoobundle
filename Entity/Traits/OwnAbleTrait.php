<?php

namespace AviatooBundle\Entity\Traits;


use AviatooBundle\Entity\Article;
use AviatooBundle\Entity\ArticleImage;
use AviatooBundle\Entity\ArticleSize;
use AviatooBundle\Entity\Interfaces\EntityInterface;
use AviatooBundle\Entity\Interfaces\OwnAbleInterface;
use AviatooBundle\Entity\User;

trait OwnAbleTrait
{
    /**
     * @param User $user
     * @return bool
     */
    public function isOwnedBy(User $user){

        if($user->isAdmin())return true;

        $user=$this->getOwner();
        $res=$user->getId() === $user->getId();

        return $res;
    }

}