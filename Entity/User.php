<?php
namespace AviatooBundle\Entity;


use AviatooBundle\Entity\Interfaces\EntityInterface;
use AviatooBundle\Entity\Traits\IdTrait;
use AviatooBundle\Entity\Traits\SyncAbleTrait;
use AviatooBundle\Entity\Traits\TimestampableTrait;
use FOS\UserBundle\Model\User as BaseUser;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;
use Symfony\Component\Validator\Constraints as Assert;
use JMS\Serializer\Annotation\Groups;
use AviatooBundle\Constants\GroupConstants as G;

class User extends BaseUser implements EntityInterface
{
    const ROLE_USER="ROLE_USER";
    const ROLE_ADMIN="ROLE_ADMIN";
    const PARAM_CONV ="USER_ID";
    use SyncAbleTrait;

    /**
     * @Assert\NotBlank(groups={User::PARAM_CONV})
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Groups({User::PARAM_CONV,G::ENTITY_OUT})
     */
    protected $id;
    /**
     * @var string $jwtToken
     * @Serializer\Type("string")
     * @Groups({G::ENTITY_OUT})
     */
    private $jwtToken;

    /**
     * @var string $refreshToken
     * @Assert\NotBlank(groups={G::LOGOUT})
     * @Serializer\Type("string")
     * @Groups({G::ENTITY_OUT, G::LOGOUT})
     */
    private $refreshToken;


    public function __construct()
    {
        $this->roles = [];
        parent::__construct();
    }

    /**
     * @return string
     */
    public function getJwtToken()
    {
        return $this->jwtToken;
    }

    /**
     * @param string $jwtToken
     */
    public function setJwtToken(string $jwtToken)
    {
        $this->jwtToken = $jwtToken;
    }

    /**
     * @return string
     */
    public function getRefreshToken()
    {
        return $this->refreshToken;
    }

    /**
     * @param string $refreshToken
     */
    public function setRefreshToken(string $refreshToken)
    {
        $this->refreshToken = $refreshToken;
    }

    public function getRole(){
        $res = self::ROLE_USER;
        $roles = array_filter($this->getRoles(),function($role){
            return $role !== self::ROLE_USER;
        });
        if(sizeof($roles) > 0){
            $res = $roles[0];
        }

        return $res;
    }
}
