<?php


namespace AviatooBundle\Entity\Interfaces;

use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpFoundation\File\UploadedFile;

/**
 * Interface FileHolderInterface
 * @package AviatooBundle\Entity\Interfaces
 */
interface FileHolderInterface
{
    /**
     * @return File
     */
    public function getFile();

    /**
     * @param UploadedFile $file
     * @return void
     */
    public function setFile(UploadedFile $file): void;
}