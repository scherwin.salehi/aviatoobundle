<?php


namespace AviatooBundle\Entity\Interfaces;


use Doctrine\ORM\EntityManager;
use AviatooBundle\Service\ParamConverter\SerializerService;
/**
 * Interface EntityInterface
 * @package AviatooBundle\Entity\Interfaces
 */
interface EntityInterface
{

    /**
     * @return array
     */
    public function getPrimaryFilter();

    /**
     * @param EntityInterface $newEntity
     * @param EntityInterface|null $defaultEntity
     * @param SerializerService $serializerService
     * @return mixed
     */
    public function sync(EntityInterface $newEntity,EntityInterface $defaultEntity=null, SerializerService $serializerService);

    /**
     * @param EntityInterface $entity
     * @return array
     */
    public function getChangedKeys(EntityInterface $entity);

    /**
     * @return array
     */
    public function getSubEntities();

    /**
     * @return string
     */
    public function getPrimaryKeys();

    /**
     * @return mixed
     */
    public function getPrimaryValue();

}