<?php


namespace AviatooBundle\Entity\Interfaces;

use Symfony\Component\HttpFoundation\File\File;

/**
 * Interface ImageInterface
 * @package AviatooBundle\Entity\Interfaces
 */
interface ImageInterface
{

    /**
     * @return string
     */
    public function getName();

    /**
     * @return string
     */
    public function getPath();

    /**
     * @return File
     */
    public function getFile();

}