<?php
namespace AviatooBundle\Entity\Interfaces;

/**
 * Interface ValidateAbleInterface
 * @package AviatooBundle\Entity\Interfaces
 */
interface ValidateAbleInterface{

    /**
     * @return void
     */
    public function validateInsert() : void;

    /**
     * @return void
     */
    public function validateUpdate() : void;
}