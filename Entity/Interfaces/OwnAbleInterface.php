<?php
namespace AviatooBundle\Entity\Interfaces;

use AviatooBundle\Entity\Article;
use AviatooBundle\Entity\Company;
use AviatooBundle\Entity\User;

/**
 * Interface OwnAbleInterface
 * @package AviatooBundle\Entity\Interfaces
 */
interface OwnAbleInterface{
    /**
     * @param User $user
     * @return bool
     */
    public function isOwnedBy(User $user);

    /**
     * @return User
     */
    public function getOwner():User;
}
