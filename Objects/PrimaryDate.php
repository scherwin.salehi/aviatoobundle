<?php
namespace AviatooBundle\Objects;




class PrimaryDate extends \DateTime
{
    public function __toString(){
        return $this->format('U');
    }

}