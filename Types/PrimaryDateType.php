<?php

namespace AviatooBundle\Types;

use AviatooBundle\Objects\PrimaryDate;
use Doctrine\DBAL\Types\DateTimeType;
use Doctrine\DBAL\Platforms\AbstractPlatform;
use Doctrine\DBAL\Types\DateType;

class PrimaryDateType extends DateType
{
    public function convertToPHPValue($value, AbstractPlatform $platform)
    {
        $dateTime = parent::convertToPHPValue($value, $platform);

        if ( ! $dateTime) {
            return $dateTime;
        }

        return new PrimaryDate('@' . $dateTime->format('U'));
    }

    public function getName()
    {
        return 'primarydate';
    }
}