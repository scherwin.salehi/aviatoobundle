<?php
namespace AviatooBundle\Constants;

/**
 * Class Groups
 * @package AviatooBundle\Groups
 */
class GroupConstants{

    const REGISTRATION = 'registration';
    const ENTITY_OUT = 'entity_out';
    const INDEX_OUT = 'index_out';
    const UPLOAD = 'upload';
    const EDIT = 'edit';
    const NEW = 'new';
    const LOGOUT = 'logout';
    const PARAM_CONV = 'param_conversion';
    const SUB_PARAM_CONV = 'sub_param_conversion';

}
